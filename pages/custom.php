<?php
/**
 * Template Name: Customize Page Template
 */
get_header();
?>

<?php get_header(); ?>
<?php
    $hero_tite = get_field('hero_title')?get_field('hero_title'):"<strong>".get_the_title()."</strong>";
    $bgcolor_left = get_field('color_code_left_end');
    $bgcolor_right = get_field('color_code_right_end');
?>

<style>
.custom_gradient{
   background: linear-gradient(315deg, <?php echo $bgcolor_right; ?> 0%, <?php echo $bgcolor_left; ?> 100%);
}
</style>
<main id="main-content" style="background-color: #FFFFFF;">
    <div class="title__non-results custom_gradient">
        <div class="wrapper">
            <div class="row">
                <div class="col-12 aligncenter">
                    <p><?= $hero_tite; ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="page__content">
        <div class="wrapper">
            <div class="row">
                <div class="col-12 aligncenter" style="line-height: 200%;">
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <?php the_content(); ?>
                        <?php endwhile; endif; ?>
                </div>
            </div>
        </div>
    </div>

    <?php get_template_part('template_parts/newsletters');  ?>
</main>
<?php get_footer(); ?>
