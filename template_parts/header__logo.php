<?php 
	$logo_img = get_field('at_logo', 'option');
?>
<div class="logo">
    <p class="text-logo"></p>

        <a href="<?php echo get_home_url(); ?>">
            <img src="<?php echo $logo_img; ?>" alt="<?= get_bloginfo('name'); ?>">
        </a>

</div>