<?php
$category_event = get_terms( array(
    'taxonomy' => 'category',
    'hide_empty' => false,
) );
$category_countries = get_terms( array(
    'taxonomy' => 'event_countries',
    'parent'   => 0,
    'hide_empty' => false,
) );
$months = getMonth();
?>
<div class="filter__event wrapper">
    <div class="wrapper">
        <div class="row">
            <div class="col-6">
                <p class="text__left"><?php _e("Where we’ll be next", DOMAIN); ?></p>
            </div>
            <div class="col-6">
                <div class="row">
                    <div class="col-4">
                        <h3 class="label__month"><?php _e("MONTH", DOMAIN); ?></h3>
                        <div class="month">
                            <?php
                            foreach($months as $item):
                            ?>
                            <span class="value__month"><?php echo $item; ?></span>
                            <?php
                            endforeach
                            ?>
                        </div>
                    </div>
                    <div class="col-4">
                        <h3><?php _e("location", DOMAIN); ?></h3>
                        <div class="box__filter--location">
                            <select class="filter__location select2" data-placeholder="City or country..." name="location" >
                                <option></option>
                                <?php
                                    $countries_array = array();
                                    if(!empty($category_countries)):
                                ?>
                                <optgroup label="COUNTRIES">
                                    <?php foreach($category_countries  as $item):
                                        $category_childs = get_terms( array(
                                            'taxonomy' => 'event_countries',
                                            'parent'   => $item->term_id,
                                            'hide_empty' => false
                                        ) );
                                        if(!empty($countries_array)){
                                            $countries_array = array_merge($countries_array, $category_childs);
                                        }else{
                                            $countries_array = $category_childs;
                                        }
                                    ?>
                                        <option value="<?php echo $item->term_id; ?>"><?php echo $item->name; ?></option>
                                    <?php endforeach ?>
                                </optgroup> <!--Countries-->
                                <?php
                                if(!empty($countries_array)):
                                ?>
                                <optgroup label="CITIES">
                                    <?php foreach($countries_array  as $item):
                                    ?>
                                        <option value="<?php echo $item->term_id; ?>"><?php echo $item->name; ?></option>
                                    <?php endforeach ?>
                                </optgroup><!--Cities-->
                                <?php
                                endif;
                                    endif;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-4">
                        <!--<h3><?php //_e("Event type", DOMAIN); ?></h3>
                        <div class="box__filter--type">
                            <select class="filter__type select2" data-placeholder="Meetup, talk..." name="type">
                                <option></option>
                                <?php //foreach($category_event  as $item): ?>
                                    <option value="<?php //echo $item->term_id; ?>"><?php //echo $item->name; ?></option>
                                <?php //endforeach; ?>
                            </select>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>