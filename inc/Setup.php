<?php
/**
 * Enqueue scripts and styles.
**/
function coingeek_scripts() {
    // Styles
    // wp_enqueue_style( 'main-style', ASSETS_PATH.'css/main04022019.css', array(), null );
    wp_enqueue_style( 'main-style', ASSETS_PATH.'css/main.css', array(), null );
	wp_enqueue_style( 'woocommerce-style', ASSETS_PATH.'css/woocommerce.css', array(), null );


    // Scripts
    wp_enqueue_script( 'main-script', ASSETS_PATH.'js/main.js', array('jquery'), null, true );
    wp_enqueue_script( 'custom', ASSETS_PATH.'js/custom.js', array('jquery'), null, true );


    wp_localize_script( 'main-script', 'wp_localize',
        array(
            'ajaxurl' => admin_url( 'admin-ajax.php' ),
            'homeurl' => get_home_url()
        )
    );
    wp_enqueue_script( 'main-script' );
}
add_action( 'wp_enqueue_scripts', 'coingeek_scripts', 100 );

/**
 * Register Menu
**/
add_action('init', 'coingeek_setup');
function coingeek_setup(){
    register_nav_menus( array(
        'athena_main_menu' => __('Main Menu', DOMAIN)
    ) );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'html5', array( 'search-form' ) );
    add_post_type_support( 'page', 'excerpt' );
    add_theme_support( 'title-tag' );
	add_theme_support( 'woocommerce' );

}
/**
 * Add Image Size
**/
if ( function_exists( 'add_image_size' ) ) {
    add_image_size( IMG_NEW_NORMAL, 380, 188, true );
    add_image_size( IMG_NEW_SIDEBAR, 280, 460, true );
    add_image_size( IMG_SLIDER_HOME, 680, 354, true );
    add_image_size( IMG_NEW_BANNER, 1920, 500, true );
    add_image_size( IMG_LATEST_NEWS, 280, 139, true );
    add_image_size( IMG_BUSINESS_THUMB, 120, 80, true );
    add_image_size( IMG_COINGEEK, 280, 220, true );
    add_image_size( IMG_COINGEEK_LARGE, 280, 460, true ); 
    add_image_size( IMG_EVENT_DETAIL, 1014, 500, true );
    add_image_size( IMG_EXCHANGE_ITEM, 90, 60, true );
    add_image_size( IMG_AUTHOR, 180, 180, true );
    add_image_size( IMG_LOCATOR, 1015, 460, true );
    add_image_size( IMG_VENTURE_DETAIL, 885, 350, true );
    add_image_size( IMG_ICON_DONATION, 100, 100);
    add_image_size( IMG_FUNDING_NORMAL, 280, 154, true);
}

// remove version from head
remove_action('wp_head', 'wp_generator');

// remove version from rss
add_filter('the_generator', '__return_empty_string');

// remove version from scripts and styles
function shapeSpace_remove_version_scripts_styles($src) {
	if (strpos($src, 'ver=')) {
		$src = remove_query_arg('ver', $src);
	}
	return $src;
}
add_filter('style_loader_src', 'shapeSpace_remove_version_scripts_styles', 9999);
add_filter('script_loader_src', 'shapeSpace_remove_version_scripts_styles', 9999);

add_filter('xmlrpc_enabled', '__return_false');