<?php
	$hero_tite = get_field('hero_title')?get_field('hero_title'):"<strong>".get_the_title()."</strong>";
	$content = $post->post_content;
	$banner_week = get_field('banner_image');
	$banner_link = get_field('banner_link');
?>
<div class="title__non-results">
	<div class="wrapper">
		<div class="row">
			<div class="col-8">
				<?=  $hero_tite; ?>
			</div>
		</div>
	</div>
</div>
<div class="include__top--list--author">
	<div class="wrapper">
		<div class="row">
			<div class="col-10">
				<div class="text">
					<?= $content; ?>
				</div>
			</div>
			<div class="col-2">
				<?php get_template_part('template_parts/sidebar__page'); ?>
			</div>
		</div>
	</div>
</div>
<div class="box__list--author">
	<div class="wrapper">
		<div class="row">
			<?php 
			 $args = array(
				'role'         => 'author',
				'orderby'      => 'meta_value',
				'meta_key'     => 'order',
				'order'        => 'ASC',
				'offset'       => 0,
				'search'       => '',
				'number'       => -1,
				'count_total'  => false,
				'fields'       => 'all',
				'who'          => '',
				'meta_query' => array(
					array(
						'key' => 'hide_author_profile_block',
						'compare' => '=',
						'value' => '0'
					)
				)
			);

				$users = get_users( $args );
				foreach ($users as $key => $use) {
					$avatar = get_field('image', 'user_'.$use->ID);
					if(!empty($avatar)){
						$avatar = $avatar['sizes']['img_author'];
					}else{
						$avatar = get_avatar_url($use->ID);
					}
					$author_link = get_author_posts_url($use->ID);
					$tagline_role = !empty(get_field('tagline_role', 'user_'.$use->ID))?get_field('tagline_role', 'user_'.$use->ID):"Author";
					$facebook_link = get_field('facebook_link', 'user_'.$use->ID);
					$twitter_link = get_field('twitter_link', 'user_'.$use->ID);
					$linkein_link = get_field('linkedin_link', 'user_'.$use->ID);
					$youtube_link = get_field('youtube_link', 'user_'.$use->ID);
				?>
				<div class="col-3">
					<div class="box__image">
						<img src="<?php echo $avatar ?>" alt="<?=$use->display_name ?>">
						<a href="<?php echo $author_link; ?>"></a>
					</div>
					<div class="info">
						<h4><?=$use->display_name ?></h4>
						<p><?= $tagline_role; ?></p>
					</div>
					<div class="soial">
						<ul>
							<?php if(!empty($facebook_link)):?>
								<li><a href="<?=$facebook_link;?>" target="_blank"><i class="icon-Facebook"></i></a></li>
							<?php endif; ?>
							<?php if(!empty($twitter_link)):?>
								<li><a href="<?=$twitter_link;?>" target="_blank"><i class="icon-Twitter"></i></a></li>
							<?php endif; ?>
							<?php if(!empty($linkein_link)):?>
								<li><a href="<?=$linkein_link;?>" target="_blank"><i class="icon-Linkedin"></i></a></li>
							<?php endif; ?>
							<?php if(!empty($youtube_link)):?>
								<li><a href="<?=$youtube_link;?>"target="_blank"><i class="icon-Youtube"></i></a></li>
							<?php endif; ?>
						</ul>
					</div>
					<a class="see__more" href="<?php echo $author_link; ?>"><?php _e("View Profile", DOMAIN); ?></a>
				</div>
				<?php
					if($key + 1 == 8 || (count($users) < 8 && $key == count($users) - 1)):
				?>
					<div class="col-12">
						<div class="banner__section">
							<?php if($banner_week): ?>
								<div class="week__banner">
									<a href="<?php echo $banner_link; ?>"><img src="<?php echo $banner_week['url'] ?>" alt=""></a>
								</div>
							<?php endif; ?>
						</div>
					</div>
				<?php
				endif;
				}
			  ?>
		</div>
	</div>
</div>
