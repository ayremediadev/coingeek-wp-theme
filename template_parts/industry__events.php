<div class="row" style="margin-top: 3em;">
	<div class="col-2"></div>
	<div class="detail-location block col-8 p-all">
		<div class="col-12">
			<p style="font-weight: 600; text-transform: uppercase; text-align: center;">Industry Events</p>
			<p style="text-align: center; margin-bottom: 1.5em;">Check out all upcoming events in the cryptocurrency world and mark your calandars.</p>
			<a href="/conference/events/"><button class="btn-default btn-yellow mt-2" style="display: block; margin: 0 auto;">All Events</button></a>
		</div>
		<div class="col-12">
			<?php $currDate = date('F d, Y');
			$eventsFeed = new WP_Query( array( 'post_type' => 'event', 
				'posts_per_page' => 6, 
				'meta_query' => array( // WordPress has all the results, now, return only the events after today's date
					array(
						'key' => 'from_date', // Check the start date field
						'value' => date('F d, Y'), // Set today's date (note the similar format)
						'compare' => '>=', // Return the ones greater than today's date
						'type' => 'DATE' // Let WordPress know we're working with date
						)
					),
				'meta_key' => 'from_date', 
				'orderby' => 'meta_value', 
				'order' => 'ASC', ) );
			$t = get_the_title();
			$startDate = get_field('from_date');
			$l = get_field('location');
			//$c = get_the_content(); 
			$e = get_field('description');
			while ( $eventsFeed->have_posts() ) : $eventsFeed->the_post(); 
					echo '<div class="col-6" style="text-align: center; margin-bottom: 1.5rem;">';
						echo '<p style="text-transform: uppercase; color: #F99E05; font-weight: 500;">' . $t . '</p>';
						echo '<p style="text-transform: uppercase;">' . $startDate . '<br/>' . $l . '</p>'; 
						//echo '<p style="font-weight: 300;">' . $e . '</p>';
					echo '</div>';
			endwhile; ?>			
		</div>
	</div>
	<div class="col-2"></div>
</div>