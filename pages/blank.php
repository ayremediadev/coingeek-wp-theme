<?php
/**
 * Template Name: Blank Template
 */
get_header();
?>

<?php get_header(); ?>
<?php
    $hero_tite = get_field('hero_title')?get_field('hero_title'):"<strong>".get_the_title()."</strong>";
?>
<main id="main-content">
    <div class="title__non-results">
        <div class="wrapper">
            <div class="row">
                <div class="col-8">
                    <p><?= $hero_tite; ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="page__content">
        <div class="wrapper">
            <div class="row">
                <div class="col-12" style="line-height: 200%;">
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <?php the_content(); ?>
                        <?php endwhile; endif; ?>
                </div>
            </div>
        </div>
    </div>

    <?php get_template_part('template_parts/newsletters');  ?>
</main>
<?php get_footer(); ?>
