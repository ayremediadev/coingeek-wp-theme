<?php
define('APP_PATH',dirname(__FILE__));
/**
 * CORE Includes
**/
include APP_PATH.'/inc/Define.php';
include APP_PATH.'/inc/Setup.php';
include APP_PATH.'/inc/admin/functions.php';
include APP_PATH.'/inc/plugins/acf/functions.php';
include APP_PATH.'/inc/plugins/wpml/functions.php';
include APP_PATH.'/inc/frontend/MainMenuWalker.php';
include APP_PATH.'/inc/frontend/CustomComments.php';
include APP_PATH.'/inc/frontend/functions.php';
include APP_PATH.'/inc/frontend/newLoadMore.php';
include APP_PATH.'/inc/frontend/exchangeFilter.php';
include APP_PATH.'/inc/frontend/eventFilter.php';
include APP_PATH.'/inc/woocommerce/functions.php';