<?php
global $wp_query;
$curauth = $wp_query->get_queried_object();
?>
<?php
$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
$id = $author->ID;
$name = $author->display_name;
$avatar = get_field('image', 'user_'.$id);
if(!empty($avatar)){
	$avatar = $avatar['sizes']['img_author'];
}else{
	$avatar = get_avatar_url($use->ID, array('size'=> 320));
}
$author_link = get_author_posts_url($id);
$tagline_role = !empty(get_field('tagline_role', 'user_'.$id))?get_field('tagline_role', 'user_'.$id):"";
$hide_author_profile_block = get_field('hide_author_profile_block', 'user_'.$id);
$facebook_link = get_field('facebook_link', 'user_'.$id);
$twitter_link = get_field('twitter_link', 'user_'.$id);
$linkein_link = get_field('linkedin_link', 'user_'.$id);
$youtube_link = get_field('youtube_link', 'user_'.$id);
$description = get_field('full_description', 'user_'.$id);
?>test
<div class="block__info--author title__non-results">
	<div class="wrapper">
		<div class="row">
			<div class="col-2">
				<div class="img">
					<img src="<?=$avatar?>" alt="<?= $name ?>">
				</div>
			</div>
			<div class="col-10">
				<div class="box__name--info">
					<h3><?= $name ?></h3>
					<?php if(!$hide_author_profile_block): ?>
						<p class="text__author"><?=$author->roles[0] ?></p>
					<?php endif; ?>
					<ul>
						<?php if(!empty($facebook_link)):?>
							<li><a href="<?=$facebook_link;?>" target="_blank"><i class="icon-Facebook"></i></a></li>
						<?php endif; ?>
						<?php if(!empty($twitter_link)):?>
							<li><a href="<?=$twitter_link;?>" target="_blank"><i class="icon-Twitter"></i></a></li>
						<?php endif; ?>
						<?php if(!empty($linkein_link)):?>
							<li><a href="<?=$linkein_link;?>" target="_blank"><i class="icon-Linkedin"></i></a></li>
						<?php endif; ?>
						<?php if(!empty($youtube_link)):?>
							<li><a href="<?=$youtube_link;?>" target="_blank"><i class="icon-Youtube"></i></a></li>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="description__author">
	<div class="wrapper">
		<div class="row">
			<div class="col-10">
				<div class="box">
					<h3 class="author__title"><?= $tagline_role; ?></h3>
					<?=$description ?>
				</div>
			</div>
			<div class="col-2">
				<?php get_template_part('template_parts/sidebar__page'); ?>
			</div>
		</div>
	</div>
</div>
