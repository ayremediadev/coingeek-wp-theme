<?php
	
/* Template Name: CG Conferences */
	
$q = new WP_Query( array( 'post_type' => 'conference', 
	'posts_per_page' => 6, 
	'post_parent' => 0, 
	'orderby' => 'menu_order') );
$term = get_queried_object();
$max_post = wp_count_posts('post')->publish;
?>
<?php get_header(); 
$date_now = date('F d, Y'); ?>
    <main id="main-content" class="new-list">
		<div class="new__banner" style="position: relative; background: url(<?php echo get_the_post_thumbnail_url(); ?>) no-repeat; background-position: center; background-size: cover; height: 500px; margin-bottom: 0;">
			<!-- <img src="<?php //echo $image_url[0]; ?>" alt=""> -->
			<div id="overlay" ></div>
			<!-- <div style="position: absolute; height: 500px; width: 100%; opacity: 0.9; background: #FA9B01;"></div> -->
			<div id="header-details" class="wrapper flex-center">
				<div class="col-12 text-center">
					<h1 class="text-upper">Coingeek Conferences</h1>
					<div class="row">
						<div class="col-2"></div>
						<div class="col-8 desc__info" >
							<p>Keeping you at the forefront of innovation, professional insights and best practices in all aspects of Bitcoin SV, cryptocurrency and blockchain technology. CoinGeek events provide a world-class forum for  knowledge sharing, debate and networking opportunities with the industry’s leaders.  CoinGeek infuses its events with positive energy, dynamic fun, and community spirit.</p>
						</div>
						<div class="col-2"></div>
					</div>
				</div>
			</div>
		</div>
        <div class="category__new">
            <div class="wrapper">
                <!--<h2 class="title"><?php //_e('Latest '.$term->name.' News', DOMAIN); ?></h2>--->
                <div class="category__new--content">
                    <?php 
					if ($q->have_posts()) : ?>
                        <div class="row mb-3">
							<?php $i = 0;
							while ($q->have_posts()) : $q->the_post();
							if(wp_get_post_parent_id(get_the_ID()) == 0){
							$date = get_field('start_date', false, false); 
							$d = new DateTime($date); 
							$df = $d->format('U');
							echo $df . ' vs ' . time() . '<br/><br/>';
                                set_query_var( 'img_new_size', IMG_NEW_NORMAL );
								if(strtotime($date_now) < strtotime($df)):
								$i++;
								?>	
								<div class="<?php if($i == 1){ echo 'col-12'; } else { echo 'col-6 cards-conf mx-auto'; } ?>" style="padding: 1em; margin-left: auto; margin-right: auto;">
									<?php 
										if(wp_get_post_parent_id(get_the_ID()) == 0){
											if($i == 1){ 
												get_template_part('template_parts/new__conference_first'); 
											} else { 
												get_template_part('template_parts/new__conference_afterfirst'); 
											} 
										} ?>
								</div>
                            <?php endif;
							}
								endwhile; ?>
                        </div>
						<?php 
							get_template_part('template_parts/old__conference'); 
							get_template_part('template_parts/industry__events'); 
						?>
                    <?php else : ?>
                        <p><?php _e('Nothing found', DOMAIN); ?></p>
                    <?php endif; ?>
                </div>
                <?php if($wp_query->max_num_pages > 1): ?>
                    <div class="load__more--block">
                        <a href="" class="btn-gradient" data-tax="<?php echo $term->taxonomy; ?>" data-category="<?php echo $term->slug; ?>" data-page="1" data-maxpage="<?php echo $wp_query->max_num_pages; ?>"><span>See More</span></a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </main>
<?php get_footer(); ?>