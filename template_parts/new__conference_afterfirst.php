<?php
	$post_title = get_the_title();
	$post_url = get_permalink();
	$post_description = get_the_excerpt();
	if(empty($img_new_size)){
		$img_new_size = IMG_NEW_NORMAL;
	}
	$image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), $img_new_size );
	if(empty($image_url)){
		$image = get_field('image_default', 'option');
		$image_url[0] = $image['sizes'][$img_new_size];
	}
	$date = human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ).' '.__( 'ago' );
	$date_actual = get_the_time( 'U' );
	$video = false;
	$terms = get_the_terms($post->ID, 'category');
	$post_type = get_post_type($post->ID);
	if($post_type == 'video'){
		$terms = get_the_terms($post->ID, 'video_category');
	}
	$content = get_the_content();
	$content = strip_tags($content);
	//$exerpt = wp_trim_words( get_the_content(), 35, ' ...' );
	$exerpt = get_post_meta($post->ID, '_yoast_wpseo_metadesc', true);
	if(preg_match("/\p{Han}+/u", $content)){
		$exerpt = wp_trim_words( get_the_content(), 2, ' ...' );
	}
	$desc = get_field('description');
?>
<div class="new cg__radius" style="box-shadow: 0 0 1.875rem 0 rgba(0,0,0,.3); padding: 3.5em 3.5em 2.5em; background: #F99E05; background: -webkit-linear-gradient(315deg,#fac161 0%,#f7961c 100%); background: -o-linear-gradient(315deg,#fac161 0%,#f7961c 100%); background: linear-gradient(135deg,#fac161 0%,#f7961c 100%);">
	<div class="col-12 new__content card-conf text-center pall-2" >
		<!-- <a href="<?php //echo get_the_permalink(); ?>" style="color: #fff; text-decoration: none;"> -->
		<h3 class="title__new text-upper" style="line-height: 1.2; font-size: 2.7em; color: #fff; font-weight: 200; padding-bottom: 10px;"><?php echo $post_title; ?></h3>
		<?php 
		$date = get_field('start_date', false, false);
		// make date object
        $date = new DateTime($date); ?> 
		<h5 class="title__new" style="font-size: 1.6em; color: #fff; text-transform: uppercase; font-weight: 700; padding-bottom: 15px;"><?php  echo $date->format('F Y'); ?></h5>
		<div style="color: #fff;"><?php echo $desc; ?></div>
		<!-- </a> -->
		<button class="btn-default btn-white mt-2" style="display:none;">Learn More</button>
	</div>
	<!-- <a href="<?php //echo $post_url; ?>" style="display: none;"><?php //_e('Read More', DOMAIN); ?></a> -->
</div>


