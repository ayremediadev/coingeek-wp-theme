<?php 
    $template = 1;
    $classes = $template?"col-12":"col-3";
    $terms = get_the_terms($post->ID, 'category');
?>
<div class="news__section">
    <div class="row">
        <div class="latest__new--left col-12">test
            <h2 class="title__section"><?php _e("latest news", DOMAIN); ?></h2>
            <div class="row">
                <?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 3,
                    'post__not_in' => array($post->ID),
                    'tax_query' => [
                        [
                            'taxonomy' => 'category',
                            'terms' => $terms[0]->term_id
                        ],
                    ],
                );
                $query = new WP_Query($args);
                if($query->have_posts()):
                    while($query->have_posts()): $query->the_post();
                        set_query_var( 'img_new_size', IMG_LATEST_NEWS );
                        ?>
                        <div class="<?php echo $classes; ?>">
                            <?php get_template_part('template_parts/new'); ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; wp_reset_query(); ?>
            </div>
        </div>
    </div>
</div>