<?php
//global $wp_query;
//query_posts('post_type' => 'conference', 'orderby' => 'post_id', 'order' => 'desc');
$term = get_queried_object();
$max_post = wp_count_posts('post')->publish;
?>
<?php get_header(); ?>
    <main id="main-content" class="new-list">
		<div class="new__banner" style="position: relative; background: url(<?php echo get_the_post_thumbnail_url(); ?>) no-repeat; background-position: center; background-size: cover; height: 500px; margin-bottom: 0;">
			<!-- <img src="<?php //echo $image_url[0]; ?>" alt=""> -->
			<div style="position: absolute; height: 500px; width: 100%; opacity: 0.9; background: #FA9B01;"></div>
			<div id="header-details" class="wrapper flex-center">
				<div class="col-12 text-center">
					<h1 style="font-size: 3rem; font-weight: 300;">Coingeek Conferences</h1>
					<div class="row">
						<div class="col-3"></div>
						<div class="col-6" style="padding: 4rem 1rem 5rem 1rem;">
							<p style="font-size: 1.2rem; font-weight: 300; color: #fff; line-height: 1;">Keeping you at the forefront of innovation, professional insights and championing best-practice in all aspects of Bitcoin SV, Cryptocurrency and Blockchain. CoinGeek events provide industry a forum for debate, knowledge sharing and networking opportunities that are unrivaled.</p>
						</div>
						<div class="col-3"></div>
					</div>
				</div>
			</div>
		</div>
        <div class="category__new">
            <div class="wrapper">
                <!--<h2 class="title"><?php //_e('Latest '.$term->name.' News', DOMAIN); ?></h2>--->
                <div class="category__new--content">
                    <?php if (have_posts()) : ?>
                        <div class="row">
							<?php $i = 0;
							while (have_posts()) : the_post();
								$i++;
                                set_query_var( 'img_new_size', IMG_NEW_NORMAL );
                                ?>	
								<div class="<?php if($i == 1){ echo 'col-12'; } else { echo 'col-6'; } ?>" style="padding: 1.5em;">
									<?php 
									if($i == 1){
										get_template_part('template_parts/new__conference_first'); 
									} else {
										get_template_part('template_parts/new__conference_afterfirst'); 
									} ?>
								</div>
                            <?php endwhile; ?>
                        </div>
						<?php 
							get_template_part('template_parts/old__conference'); 
							get_template_part('template_parts/industry__events'); 
						?>
                    <?php else : ?>
                        <p><?php _e('Nothing found', DOMAIN); ?></p>
                    <?php endif; ?>
                </div>
                <?php if($wp_query->max_num_pages > 1): ?>
                    <div class="load__more--block">
                        <a href="" class="btn-gradient" data-tax="<?php echo $term->taxonomy; ?>" data-category="<?php echo $term->slug; ?>" data-page="1" data-maxpage="<?php echo $wp_query->max_num_pages; ?>"><span>See More</span></a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </main>
<?php get_footer(); ?>