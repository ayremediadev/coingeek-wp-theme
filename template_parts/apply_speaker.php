<?php
  //$short_code = get_field('shortcode', 'option');
?>
<div class="box__applyS" id="apply-to-speak">
  <div class="wrapper">
  	<div class="row text-center p-all">
		<h4 class="text-upper f-bold mb-2">Apply to speak</h4>
		<p>Please read the form carefully and fill in all the fields. We will reject submissions with incomplete fields.</p>
	</div>
    <div class="row">
      <div class="include__formApply" >
		<div class="col-12">
			<div class="col-2"></div>
			<div class="col-8">
				<?php echo do_shortcode( '[contact-form-7 id="135806" title="Apply to speak"]' ); ?>
			</div>
			<div class="col-2"></div>
		</div>
      </div>
    </div>
  </div>
</div>