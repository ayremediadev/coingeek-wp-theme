<?php
/**
 * Template Name: Page Home
 */
get_header();

?>
<main id="main-content">
	<?php get_template_part('template_parts/home__slider'); ?>
	<?php get_template_part('template_sections/home__section--one'); ?>
	<?php get_template_part('template_sections/home__coingeek'); ?>
	<?php get_template_part('template_sections/home__section--two'); ?>
	<?php get_template_part('template_sections/home__footer'); ?>
</main>

<!--Start Pull HTML here-->
<!--END  Pull HTML here-->
<?php get_footer(); ?>