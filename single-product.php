
<?php get_header(); ?>

<main id="main-content" class="page-conference">

    <div class="wrapper">
        <div class="row">
            <!-- <div class="col-1"></div> -->
            <div class="col-12">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <div class="new__container">
                        <div class="content">
                            <?php 
                            the_content(); 
                            ?>
                        </div>
                    </div>
                    
                <?php endwhile; endif; ?>
            </div>
            <!-- <div class="col-1"></div> -->
        </div>
    </div>
    <?php get_template_part('template_parts/newsletters');  ?>

</main>
<?php get_footer(); ?>
