<?php
/**
 * Template Name: Page BuyBSV
 */
get_header();
$hero_tite = get_field('hero_title')?get_field('hero_title'):"<strong>".get_the_title()."</strong>";
$content = $post->post_content;
?>
    <main id="main-content">
        <div class="page__non-results">
            <div class="title__non-results">
                <div class="wrapper">
                    <?= $hero_tite; ?>
                </div>
            </div>
            <div class="include">
                <div class="main__content" id="bsvIframe">
                    <div class="wrapper" id="wallets">
                        <div class="row">
                            <div class="col-2"></div>
                                <div class="col-lg-8">
                                    <?php echo $content; ?>
                                </div>
                            <div class="col-2"></div>
                        </div>
                    </div>
                </div>
                <div class="content__resources">
                    <div class="has__results">
                        <div class="wrapper">
                            <div class="row">
                                <p style="text-transform: uppercase; text-align: center;" class="mb-2"><strong>Wallets</strong></p>

                                <?php
                                //echo do_shortcode('[facetwp template="buybsv"]');
                                if( have_rows('wallet') ):

								 	// loop through the rows of data
								    while ( have_rows('wallet') ) : the_row(); 
								    	//print_r( );?>
								    	<div class="col-lg-3 text-center">
								    		<a href="<?php the_sub_field('website_link') ?>"><img class="img-fluid aligncenter" src="<?php the_sub_field('wallet_image') ?>" style="width: 240px; border-radius: 0 .625rem;"></a>
								    		<a class="whybsv_label" href="<?php the_sub_field('website_link') ?>" style="font-size: 1rem; font-weight: bold;"><?php the_sub_field('website_name') ?></a>
								    		<div class="download-app col-lg-12">
								    			<div class="row my-1 pt-1">
									    			<div class="col-lg-6 text-center">
									    				<a href="<?php the_sub_field('ios_app_store_link'); ?>" target="_blank" rel="noopener"><img class="img-fluid aligncenter" src="https://coingeek.com/wp-content/uploads/2019/05/global-appstore.png"></a>
									    			</div>
									    			<div class="col-lg-6 text-center">
									    				<a href="<?php the_sub_field('google_play_store_link'); ?>" target="_blank" rel="noopener"><img class="img-fluid aligncenter" src="https://coingeek.com/wp-content/uploads/2019/05/global-playstore.png"></a>
									    			</div>
								    			</div>
								    		</div>
								    	</div>
								        
								 <?php   endwhile;

								else :

								    // no rows found

								endif;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php get_template_part('template_parts/newsletters') ?>
        </div>
    </main>

    <!--Start Pull HTML here-->
    <!--END  Pull HTML here-->
<?php get_footer(); ?>