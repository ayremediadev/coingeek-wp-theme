<p style="font-weight: 600; text-transform: uppercase; text-align: center; margin-bottom: 1.5em;">Past Events</p>
<div class="row">
	<div class="col-1"> </div>
	<div class="col-10">
		<table style="width: 100%;">
			<tbody>
				<tr style="background: #e1e1e1; color: #eab300;">
					<th style="padding: 1em;">CONFERENCE</th>
					<th style="padding: 1em;">VENUE</th>
					<th style="padding: 1em;">DATE</th>
				</tr>
				<?php $date_now = date('F d, Y'); 
					$q = new WP_Query( array( 'post_type' => 'conference', 
					'posts_per_page' => 6, 
					'post_parent' => 0,
					'orderby' => 'menu_order', 
					'order' => 'DESC') );
					while ($q->have_posts()) : $q->the_post(); 
					$t = get_the_title(); 
					$l = '<h5>' . get_field('city') . '<br/>' . get_field('country') . '</h5>'; 
					$v = get_field('venue_name'); 
					if($v){
						$where = $v; 
					} else {
						$where = $l;
					}
					$date = get_field('start_date', false, false); 
					$d = new DateTime($date); 
					$df = $d->format('F d, Y');
						if(strtotime($date_now) > strtotime($df)): ?>
							<tr>
								<td style="padding: 1em;"><strong><?php echo $t; ?></strong></td>
								<td style="padding: 1em;"><?php echo $where; ?></td>
								<td style="padding: 1em;"><?php echo $df; ?></td>
							</tr>
						<?php endif; 
					endwhile; ?>
			</tbody>
		</table>
	</div>
	<div class="col-1"> </div>
</div>

