<?php
/**
 * Template Name: Cart
 */
get_header();
$hero_tite = get_field('hero_title')?get_field('hero_title'):"<strong>".get_the_title()."</strong>";
?>
<main id="main-content" class="page-conference">


    <div class="wrapper">
        <div class="row">
            <div class="new__wrapper <?php if($template) echo "col-12" ?>">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <div class="new__container">
                    
                        <h2 class="title"><?php the_title(); ?></h2>
                        <?php if (sizeof(WC()->cart->get_cart()) != 0) : ?>
                        <div class="cart-link"><a href="<?php echo get_permalink(142730) ?>">Go back to ticket selection</a></div>
                        <?php endif; ?>
                        <div class="content">
                            <?php the_content(); ?>
                        </div>
                    </div>
                    
                <?php endwhile; endif; ?>
            </div>
        </div>
    </div>
    <?php get_template_part('template_parts/newsletters');  ?>

</main>

<?php get_footer(); ?>
