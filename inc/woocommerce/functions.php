<?php

/**
 * Change a currency symbol
 */
add_filter('woocommerce_currency_symbol', 'change_existing_currency_symbol', 10, 2);

function change_existing_currency_symbol( $currency_symbol, $currency ) {
     switch( $currency ) {
          case 'CAD': $currency_symbol = 'CAD$ '; break;
     }
     return $currency_symbol;
}



/**
 * Remove unneeded content
 */
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

/**
 * Hide coupon form
 */
// hide coupon field on cart page
//function hide_coupon_field_on_cart( $enabled ) {
//    if ( is_cart() ) {
//        $enabled = false;
//    }
//    return $enabled;
//}
//add_filter( 'woocommerce_coupons_enabled', 'hide_coupon_field_on_cart' );

//remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
//add_action( 'woocommerce_after_checkout_form', 'woocommerce_checkout_coupon_form' );


/**
 * Remove image zoom and gallery
 */
function custom_single_product_image_html( $html, $post_id ) {
    $post_thumbnail_id = get_post_thumbnail_id( $post_id );
    return get_the_post_thumbnail( $post_thumbnail_id, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ) );
}
add_filter('woocommerce_single_product_image_thumbnail_html', 'custom_single_product_image_html', 10, 2);

/**
 * Cleanup checkout fields 
 */  

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

function custom_override_checkout_fields( $fields ) {
    //unset($fields['order']['order_comments']);
    $fields['order']['order_comments']['default'] = 'Online Store';
    // unset($fields['billing']['billing_address_1']);
    // unset($fields['billing']['billing_address_2']);
    // unset($fields['billing']['billing_city']);
    // unset($fields['billing']['billing_postcode']);
    // unset($fields['billing']['billing_country']);
    // unset($fields['billing']['billing_state']);

    $fields['billing']['billing_phone']['label'] = 'Mobile phone';
    $fields['billing']['billing_phone']['required'] = false;

    $fields['billing']['billing_title'] = array(
        'label'     => __('Job Title', 'woocommerce'),
        'placeholder'   => _x('Job Title', 'placeholder', 'woocommerce'),
        'required'  => false,
        'class'     => array('form-row-wide'),
        'clear'     => true
    );
    $fields['billing']['billing_website'] = array(
        'label'     => __('Website', 'woocommerce'),
        'placeholder'   => _x('Website', 'placeholder', 'woocommerce'),
        'required'  => false,
        'class'     => array('form-row-wide'),
        'clear'     => true
    );
    $fields['billing']['billing_copy_to'] = array(
        'type'      => 'checkbox',
        'label'     => __('Copy details to Attendee 1', 'woocommerce'),
        'required'  => false,
        'class'     => array('form-row-wide'),
        'clear'     => true
    );


    $fields['billing']['billing_first_name']['placeholder'] = 'First Name';
    $fields['billing']['billing_last_name']['placeholder'] = 'Last Name';
    $fields['billing']['billing_email']['placeholder'] = 'Email';
    $fields['billing']['billing_company']['placeholder'] = 'Company Name';
    $fields['billing']['billing_phone']['placeholder'] = 'Mobile Phone';
    $fields['billing']['billing_city']['placeholder'] = 'Town or City';
    $fields['billing']['billing_postcode']['placeholder'] = 'Zip Code';

    $fields['billing']['billing_first_name']['label'] = 'First Name';
    $fields['billing']['billing_last_name']['label'] = 'Last Name';
    $fields['billing']['billing_email']['label'] = 'Email';
    $fields['billing']['billing_company']['label'] = 'Company Name';
    $fields['billing']['billing_phone']['label'] = 'Mobile Phone';


    $fields['billing']['billing_first_name']['priority'] = 5;
    $fields['billing']['billing_last_name']['priority'] = 10;
    $fields['billing']['billing_email']['priority'] = 15;
    $fields['billing']['billing_phone']['priority'] = 20;

    $fields['billing']['billing_address_1']['priority'] = 30;
    $fields['billing']['billing_address_2']['priority'] = 35;
    $fields['billing']['billing_city']['priority'] = 40;
    
    $fields['billing']['billing_country']['priority'] = 40;
    $fields['billing']['billing_postcode']['priority'] = 50;
    $fields['billing']['billing_state']['priority'] = 50;
    

    $fields['billing']['billing_title']['priority'] = 90;
    $fields['billing']['billing_company']['priority'] = 90;
    $fields['billing']['billing_website']['priority'] = 95;
    $fields['billing']['billing_copy_to']['priority'] = 100;

    $fields['billing']['billing_postcode']['class'] = array('form-row-first');
    $fields['billing']['billing_state']['class'] = array('form-row-last');

    return $fields;
}

/**
 * Disasble SelectWoo
 */

add_action( 'wp_enqueue_scripts', 'dequeue_stylesandscripts_select2', 100 );
 
function dequeue_stylesandscripts_select2() {
    if ( class_exists( 'woocommerce' ) ) {
        wp_dequeue_style( 'selectWoo' );
        wp_deregister_style( 'selectWoo' );
 
        wp_dequeue_script( 'selectWoo');
        wp_deregister_script('selectWoo');
    } 
} 


/**
 * Display field value on the order edit page
 */
 
add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );

function my_custom_checkout_field_display_admin_order_meta($order){
    echo '<p><strong>'.__('Job Title').':</strong> ' . get_post_meta( $order->get_id(), '_billing_title', true ) . '</p>';

    echo '<p><strong>'.__('Website').':</strong> ' . get_post_meta( $order->get_id(), '_billing_website', true ) . '</p>';
}


/**
 * Update the order meta with field value
 */
add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta' );

function my_custom_checkout_field_update_order_meta( $order_id ) {
    if ( ! empty( $_POST['billing_title'] ) ) {
        update_post_meta( $order_id, 'Job Title', sanitize_text_field( $_POST['billing_title'] ) );
    }
    if ( ! empty( $_POST['billing_website'] ) ) {
        update_post_meta( $order_id, 'Website', sanitize_text_field( $_POST['billing_website'] ) );
    }
}


/**
 * Autocomplete orders
 */

 add_action( 'woocommerce_thankyou', 'wc_auto_complete_paid_order', 20, 1 );
 function wc_auto_complete_paid_order( $order_id ) {
     if ( ! $order_id )
         return;
     $order = wc_get_order( $order_id );

     if ( in_array( $order->get_payment_method(), array( 'bacs', 'cod', 'cheque', 'coingate', '' ) ) ) {
         return;
     } else {
         $order->update_status( 'completed' );
     }
 }


/**
 * Copy details to Attendee 1
 */

add_action( 'woocommerce_before_checkout_form', 'copy_to_attendee' );
function copy_to_attendee() {
    ?>
    <script type="text/javascript">
    jQuery(function($){

        $('input#billing_copy_to').prop('checked', false);
        var a = 'input#billing_copy_to';

        $(a).change(function(){
            if ( $(this).prop('checked') === true ) {
                document.querySelector('[id$="_attendee_1__1"]').value = document.getElementById('billing_first_name').value;
                document.querySelector('[id$="_attendeelastname_1__1"]').value = document.getElementById('billing_last_name').value;
                document.querySelector('[id$="_attendeeemail_1__1"]').value = document.getElementById('billing_email').value;
            }
            else if ( $(this).prop('checked') !== true ) {
                document.querySelector('[id$="_attendee_1__1"]').value = "";
                document.querySelector('[id$="_attendeelastname_1__1"]').value = "";
                document.querySelector('[id$="_attendeeemail_1__1"]').value = "";
            }
        });
    });
    </script>
    <?php
}



