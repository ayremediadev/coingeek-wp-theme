<?php
/**
 * Ajax filter Resource
 */
//get news by ajax
add_action( 'wp_ajax_filter_resource', 'filter_resource' );
add_action( 'wp_ajax_nopriv_filter_resource', 'filter_resource' );

function filter_resource(){
    global $wp_query;
    $tool = !empty($_POST['tool'])?$_POST['tool']:null;
    if(empty($tool)){
        return false;
        wp_die();
    }
    $local = !empty($_POST['local'])?$_POST['local']:null;
    $curency = !empty($_POST['curency'])?$_POST['curency']:null;
    $device = !empty($_POST['device'])?$_POST['device']:null;
    $parameter_array = array('relation' => 'AND');
    if(!empty($local)){
        $parameter_array[] = array(
            'taxonomy' => 'exchange_country',
            'field'    => 'term_id',
            'terms'    => $local,
        );
    }
    if(!empty($curency)){
        $parameter_array[] = array(
            'taxonomy' => 'exchange_currency',
            'field'    => 'term_id',
            'terms'    => $curency,
        );
    }
    if(!empty($device)){
        $parameter_array[] = array(
            'taxonomy' => 'exchange_devices',
            'field'    => 'term_id',
            'terms'    => $device,
        );
    }
    $args = array(
        'post_type' => array(
            $tool
        ),
        'post_status' => array(
            'publish'
        ),
        'order' => 'DESC',
        'orderby' => 'date',
        'tax_query' => array(
            'relation' => 'AND'
        ),
        'posts_per_page' => -1,
    );
    $args['tax_query'] = $parameter_array;
    $query = new WP_Query($args);
    ob_start();
    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();
            ?>
            <?php get_template_part('template_parts/exchange__item'); ?>
            <?php
        }
    }else{
        return false;
        wp_die();
    }
    $response = ob_get_contents();
    ob_end_clean();
    echo $response;
    wp_die();
}
?>