<?php
/**
 * Template Name: Locator
 */
get_header();
?>
<main id="main-content">
	<?php get_template_part('template_parts/locator'); ?>
	<?php get_template_part('template_parts/suggest__event'); ?>
	<?php get_template_part('template_sections/home__footer'); ?>
</main>

<?php get_footer(); ?>