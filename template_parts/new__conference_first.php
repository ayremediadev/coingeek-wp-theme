<?php
	$post_title = get_the_title();
	$post_url = get_permalink();
	$post_description = get_the_excerpt();
	if(empty($img_new_size)){
		$img_new_size = IMG_NEW_NORMAL;
	}
	//$image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), $img_new_size );
	$image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array('300','300') );

	if(empty($image_url)){
		$image = get_field('image_default', 'option');
		$image_url[0] = $image['sizes'][$img_new_size];
	}
	$date = human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ).' '.__( 'ago' );
	$date_actual = get_the_time( 'U' );
	$video = false;
	$terms = get_the_terms($post->ID, 'category');
	$post_type = get_post_type($post->ID);
	if($post_type == 'video'){
		$terms = get_the_terms($post->ID, 'video_category');
	}
	$content = get_the_content();
	$content = strip_tags($content);
	//$exerpt = wp_trim_words( get_the_content(), 35, ' ...' );
	$exerpt = get_post_meta($post->ID, '_yoast_wpseo_metadesc', true);
	if(preg_match("/\p{Han}+/u", $content)){
		$exerpt = wp_trim_words( get_the_content(), 2, ' ...' );
	}
	$desc = get_field('description', false, false);
?>
<style>
		.new .col-6.new__content {
			padding: 2.5em 2.5em 2.5em 4.5em; 		
  		}
	@media only screen and (max-width: 810px) {
  		.new .col-6.new__content {
  			padding: 2.5em; 	
  		}
}
</style>
<div class="new cg__radius" style="box-shadow: 0 0 1.875rem 0 rgba(0,0,0,.1); padding: 2.5em;">
	<div class="col-6 image cg__radius" style="min-height: 400px; background: url(<?php echo $image_url[0]; ?>); background-size: cover; background-position: center;">
		<?php if($post_type == 'video' && !empty($image_url)): ?>
			<span class="icon-Dropdown"></span>
		<?php endif; ?>
	</div>
	<div class="col-6 new__content" style="text-align: center;">

		<h3 class="title__new t_first text-upper y-col pt-2" style="line-height: 1.2; font-size: 3em; font-weight: 200; padding-bottom: 15px;"><?php echo $post_title; ?></h3>

		<?php 
		$date = get_field('start_date', false, false);
		// make date object
        $date = new DateTime($date); ?> 

		<h5 class="title__new" style="color: #111; text-transform: uppercase; font-weight: 700"><?php  echo $date->format('F Y'); ?></h5>

		<hr style="margin: 3em 0 3em 0; borger-top: 1px solid #eee"/>

		<?php echo $desc; ?>

		<!-- Link to pre-registration -->
		<p class="mt-3"><a  class="btn-default btn-yellow mt-2 mx-auto"  style="font-weight: 500; font-size: .7rem; margin-left: auto; margin-right: auto;" href="/seoul-conference"><?php _e('Read More', DOMAIN); ?></a></p>

		<!-- Link to conference page -->
		<!-- <p><a  class="btn-default btn-yellow mt-2 mx-auto"  style="font-weight: 300; margin-left: auto; margin-right: auto;" href="<?php echo $post_url; ?>"><?php _e('Read More', DOMAIN); ?></a></p> -->

		<!-- <button class="btn-default btn-yellow mt-2" style="display: none;">Learn More</button> -->
	</div>
	<!-- <a href="<?php //echo $post_url; ?>"><?php //_e('Read More', DOMAIN); ?></a> -->
</div>


