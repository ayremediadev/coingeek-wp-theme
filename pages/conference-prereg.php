<?php
/**
 * Template Name: Conference Pre-registration
 */
get_header();
?>
<main id="main-content">
    <div id="main-header" class="new__banner" style="position: relative; background: url('https://coingeek.ga/wp-content/uploads/2019/05/seoul.jpg') no-repeat; background-position: center -100px; background-size: cover; height: 600px; margin-bottom: 0;">

        <div id="overlay" ></div>
        <div id="header-details" class="wrapper flex-center">
            <div class="text-center">
                <h1 class="mb-2" style="line-height: 1.2"><?php the_title(); ?></h1>
                <h5 class="mb-2">OCTOBER 2019</h5>
            </div>
        </div>

    </div>


    <div class="prereg new__container" style="margin-top: -200px">
        <div class="content" style="margin-bottom: 0; padding-bottom: 1.875rem;">
            <div class="row">
                <div class="col-1"></div>
                <div class="block col-10 p-all" style="background: #fff">
                    <?php the_content(); ?>
                </div>
                <div class="col-1"></div>
            </div> 
            
        </div>
    </div>



<!--      <div class="wrapper" style="clear: both;">
        <div class="row">
                <div class="new__wrapper col-12" style="text-align: left;">
                        <div class="new__container" style="margin-top: -200px">
                            <div class="content">

                                <div id="about" class="active confTab">
                                    <div class="row">
                                        <div class="col-1"></div>
                                        <div class="detail-location block col-10 p-all" style="background: #fff">
                                            <?php the_content(); ?>
                                        </div>
                                        <div class="col-1"></div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
 <?php get_template_part('template_parts/newsletters');  ?>
</main>
<?php get_footer(); ?>