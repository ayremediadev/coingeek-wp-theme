<?php
	
	$id = get_the_ID();
	$p = wp_get_post_parent_id($id);
	$gp = wp_get_post_parent_id($p);
	if($p == 0 || $id == 22893){
		$q_id = get_the_ID(); 
	} elseif($gp !== 0) { 
		$q_id = $gp;
		} else {
		$q_id = $p;
	}
	$post = get_post($id);
	$slug = $post->post_name;
	
	$descVid = get_field('description_video', $q_id);
	$desc = get_field('description', $q_id);
    $gmap = get_field('google_map_embed', $q_id);
    $venue = get_field('venue_name', $q_id);
    $location = '<h5>' . get_field('city', $q_id) . '<br/>' . get_field('country', $q_id) . '</h5>';
    $faqs = get_field('faqs', $q_id); 

    $speakers = get_field('speakers', $q_id);
    $form = get_field('contact_form', $q_id);
    $sponsors = get_field('sponsors', $q_id);
    $keep_info_title = get_field('title_kf', $q_id);
    $keep_info_desc = get_field('description_kf', $q_id);
    $keep_info_cf = get_field('contact_form_shortcode', $q_id);

?>
<?php get_header(); ?>
<main id="main-content" class="<?php if($template) echo "has-sidebar" ?>">
    <div id="main-header" class="new__banner" style="position: relative; background: url(<?php echo get_the_post_thumbnail_url($q_id); ?>) no-repeat; background-position: center; background-size: cover; height: 500px; margin-bottom: 0;">

        <div id="overlay" ></div>
        <div id="header-details" class="wrapper flex-center">
            <div class="col-2"></div>
            <div class="col-8 text-center">
                <h1 class="mb-2">
					<?php 
						if($p !== 0){
							echo get_the_title($q_id); 
						} else {
							echo get_the_title();
						}
					?>
				</h1>
				<?php 
                $confLoc = get_field('country', $q_id);
                $date = get_field('start_date', $q_id, false, false);
                // make date object
                $date = new DateTime($date); 
                ?> 
                <h5 class="mb-2"> <?php  echo $date->format('M Y'); ?> | <?php  echo $confLoc ; ?> </h5>
                <a href="https://coingeek.com/conferences/toronto-conference/buy-tickets/" class="btn-default btn-white register_btn" style="margin: 0 auto;"> Register Now</a>
            </div>
            <div class="conf-loc col-2"> 
                
            </div>
        </div>
    </div>
	
	<?php //print_r(get_queried_object()); ?>

    <div id="hackathon_header" class="new__banner" style="position: relative; background-position: center; background-size: cover; height: 500px; margin-bottom: 0;">
        <!-- <img src="<?php //echo $image_url[0]; ?>" alt=""> -->
        <div id="overlay" ></div>
        <div id="header-details" class="wrapper flex-center">
            <div class="col-2"></div>
            <div class="col-8 text-center">
            	<?php $img_h = get_field('header_image', $q_id); 
            		  $desc_h = get_field('header_description', $q_id);
            	?>
              <div class="col-6" id="head_img">
              	
              	<img src="<?php echo $img_h; ?>">
              </div>
              <div class="col-6" id="head_desc">
              	<?php echo $desc_h; ?>
              </div>
            </div>
            <div class="col-2"></div>
        </div>
    </div>

    <div id="nav_conference">
        <div class="wrapper" style="text-align: center;">
			<div class="menu-conference_submenu-container">
			<ul id="menu-conference_submenu" class="menu">
				<?php 
				$c = get_the_permalink(); 
				//echo '$c = ' . $c . ' <br/>'; 
				$aboutLnk = get_the_permalink($q_id); ?>
				<?php
				$args = array(
					'post_type'      => 'conference', //write slug of post type
					'posts_per_page' => -1,
					'post_parent'    => $q_id, //place here id of your parent page
					'order'          => 'ASC',
					'orderby'        => 'menu_order'
				);
				$ch = new WP_Query( $args ); 
				if( $ch->have_posts() ): ?>
					<li id="menu-item-120456" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-120456"><a href="<?php echo $aboutLnk; ?>" data-title="About">About</a></li>
					<?php while( $ch->have_posts() ) : $ch->the_post(); ?>
						<li id="menu-item-<?php echo get_the_ID(); ?>" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-<?php echo get_the_ID(); if( get_the_permalink() == $c ){ echo ' active'; }?>"><a href="<?php echo get_the_permalink(); ?>" data-title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
					<?php 
					endwhile;
				endif;
				wp_reset_query();
				?>			
			</ul>
			</div>		
        </div>
    </div>
    
    <div class="wrapper" style="clear: both;">
        <?php if($template): ?>
        <div class="row">
            <?php endif; ?>
                <div class="new__wrapper <?php if($template) echo "col-12" ?>" style="text-align: left;">
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <div class="new__container" style="margin-top: 2em;">
                            <div class="content">

                                <div id="about" class="active confTab">
                                    <div class="row">
                                        <div class="col-2"></div>
                                        <div class="detail-location block col-8 p-all">
                                            <div class="col-12">
                                                <p> <?php echo $descVid; ?> </p>
                                                <p> <?php echo $desc; ?> </p>
                                            </div>
                                            <div class="col-12">
                                                <h4 style="border-bottom: 2px #e1e1e1 solid; font-weight: bold;">LOCATION</h4>
                                            </div>
                                            <div class="col-6">
                                                <?php echo $venue; ?>
                                                <br/>
                                                <p style="display:none;"><a style="color: #000; font-weight: bold;">PLAN YOUR TRIP</a><br/>
                                                <a style="color: #000; font-weight: bold;">GET DIRECTIONS</a></p>
                                            </div>
                                            <div class="col-6">
                                                <?php echo $gmap; ?>
                                            </div>
                                        </div>
                                        <div class="col-2"></div>
                                    </div>
                                    <div id="summit-icons" class="col-12" style="display: none;">

                                        <h3>Summit in Numbers</h3>

                                        <div class="col-2">
                                            <div class="block"> <img src="https://coingeek.ga/wp-content/uploads/2019/03/Attendees.png"></div>
                                            <p class="numbers">400</p>
                                            <p class="desc"> Attendees</p>
                                        </div>
                                       <div class="col-2">
                                            <div class="block"> <img src="https://coingeek.ga/wp-content/uploads/2019/03/Executives.png"></div>
                                            <p class="numbers">60%</p>
                                            <p class="desc"> executives </p>
                                        </div>
                                        <div class="col-2">
                                            <div class="block"> <img src="https://coingeek.ga/wp-content/uploads/2019/03/Speakers.png"></div>
                                            <p class="numbers">10</p>
                                            <p class="desc"> speakers </p>
                                        </div>
                                        <div class="col-2">
                                            <div class="block"> <img src="https://coingeek.ga/wp-content/uploads/2019/03/MediaPartners.png"></div>
                                            <p class="numbers">20</p>
                                            <p class="desc"> Media Partners </p>
                                        </div>
                                        <div class="col-2">
                                            <div class="block"> <img src="https://coingeek.ga/wp-content/uploads/2019/03/Sponsors.png"></div>
                                            <p class="numbers"> 10 +</p>
                                            <p class="desc"> Sponsors</p>
                                        </div>
                                        
                                    </div>
                                    
                                </div>

                                <div id="agenda" class="confTab">
                                    <div class="col-10">
                                        <h4 style="font-weight: bold; text-transform: uppercase; display: none;">Program of the <?php the_title(); ?> conference</h4>
                                        <p><?php $a_intro = get_field('intro_for_agenda', $q_id); echo $a_intro;?></p>
                                        <div class="tab">
                                        <?php 
                                            $no_days = get_field('n_days', $q_id);
                                            $date2 = get_field('start_date', $q_id, false, false);
                                                for ($i=0; $i < $no_days; $i++) { 
                                                    $date = new DateTime($date2); 
                                                    $date->modify('+'.$i.' day'); 
                                                    $n = $i + 1;
                                                    ?>

                                                    <button class="tablinks" onclick="openCity(event, 'day_<?php echo $n;?>')" > <?php echo $date->format('dS M Y'); ?></button>
                                        <?php   } ?>
                                        </div>

                                        <!-- Tab content -->
                                        <?php for ($j=1; $j <= $no_days; $j++) { ?>
                                                <div id="day_<?php echo $j;?>" class="tabcontent">
                                                    <?php 
                                                    $d = get_field('day_'.$j, $q_id); 
                                                    
                                                    $day_title = '<h4>' . $d['day_title'] . '</h4>'; 
                                                    $day_desc = '<p>' . $d['day_description'] . '</p>'; 
                                                    $day_agenda = $d['agenda_day'];
                                                    $agenda_table = $day_agenda['agenda_table']; 
                                                    
                                                    echo $day_title; 
                                                    echo $day_desc; 
                                                    echo $agenda_table; 
                                                    ?>
                                                </div>
                                        <?php } ?>
                                        <script>
                                            function openCity(evt, cityName) {
                                              var i, tabcontent, tablinks;
                                              tabcontent = document.getElementsByClassName("tabcontent");
                                              for (i = 0; i < tabcontent.length; i++) {
                                                tabcontent[i].style.display = "none";
                                              }
                                              tablinks = document.getElementsByClassName("tablinks");
                                              for (i = 0; i < tablinks.length; i++) {
                                                tablinks[i].className = tablinks[i].className.replace(" active", "");
                                              }
                                              document.getElementById(cityName).style.display = "block";
                                              //evt.currentTarget.className += " active";
                                            }
                                        </script>
                                        
                                    </div>
                                    <div class="col-2 has-sidebar">
                                        <ul class="side-t">
                                            <li>
                                                <p>Have a question?</p>
                                                <i class="far fa-arrow-right"></i> <a href="https://coingeek.com/contact-us/"> Contact us</a>
                                            </li>
                                            <div style="display:none;">
                                            <?php
                                                $date3 = get_field('start_date', $q_id, false, false);
                                                for ($i=0; $i < $no_days; $i++) {  
                                                    $date = new DateTime($date3);
                                                    $date->modify('+'.$i.' day'); ?>
                                                    <li>
                                                        <p><?php echo $date->format('dS M'); ?></p>
                                                        <i class="far fa-arrow-right"></i> <a href="#">Discover Program</a>
                                                    </li>
                                            <?php } ?>
                                            </div>
                                        </ul>
                                    </div>
                                </div>

                                <div id="speakers" class="confTab">
									<?php if($slug == 'speakers'){ ?>
										<div class="col-12 all-speakers" style="min-height: 1px;">
											<div class="col-10 ">
												<div class="row tab-desc mb-2" style="display:none;">
													<h4><?php the_title(); ?> Speaker's team:</h4>
													<h4 class="mb-2">Chief Scientist, founder, ceo, cso and more</h4>
													<p> <?php $s_desc = get_field('speakers_intro', $q_id); echo $s_desc;?></p>
												</div>
											
												<div class="row">
												<?php
													if($slug == 'speakers'){
														foreach ($speakers as $speaker) {
															speaker_info($speaker->ID);
														} 
													}
												?>
												</div>
												
											</div>
											<div class="col-2 has-sidebar">
												<ul class="side-t">
													<li>
														<p>Have a question?</p>
														<i class="far fa-arrow-right"></i> <a href="/contact-us"> Contact us</a>
													</li>
													<li style="display: none;">
														<p>Apply to speak</p>
														<i class="far fa-arrow-right"></i> <a href="#apply-to-speak"> Fill out form</a>
													</li>
												</ul>
											</div>
										</div>
									<?php } ?>
									
                                    <?php 
									foreach ($speakers as $speaker) {
										if($slug == $speaker->post_name){
											speaker_profile($speaker->ID); 
										}
									} ?>
                                    
                                </div>
                                <!-- <div id="registration" class="confTab">
                                	<div class="col-2"></div>
                                    <?php 
                                    	$formT = get_field('tickets_form', $q_id); ?>
                                    	<div class="col-8">
                                    		<div class="heading text-center">
                                    			<p class="main-title f-bold">COINGEEK TORONTO PRE-REGISTRATION</p>
                                    			<p class="des">Tickets will go on sale on March 18th. Pre-register now by filling out the form below to receive updates on the conference.</p></div>
                                    		<div class="form-default">
                                    <?php  	echo do_shortcode($formT);?>
                                    		</div>
                                    	</div>
                                    	<div class="col-2"></div>
                                </div> -->

                                <div id="registration" class="confTab">
                                        <div class="col-1"></div>
                                        <div class="detail-registration block col-10 p-all">
                                            <div class="new__container">
                                                <div class="content">
                                                    <?php echo do_shortcode('[product_page id="142730"]'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-1"></div>
                                </div>

                                <div id="keep-informed" class="confTab ">
                                    <div class="col-10">
                                        <div class="devday-hero">
                                            <img src="https://coingeek.com/wp-content/uploads/2019/05/DevDay_Image.jpg">
                                        </div>
                                        <div class="text-center">
                                            <p><?php echo $keep_info_desc; ?></p>
                                        </div>
                                        <div class="col-12 page-template-contact-news">
                                            <div class="col-2"></div>
                                            <div class="form-default col-8 box__contact--news">
                                            <?php echo do_shortcode($keep_info_cf); ?>
                                            </div>  
                                            <div class="col-2"></div>
                                        </div>
                                        
                                    </div>
                                    <div class="col-2 has-sidebar">
                                        <ul class="side-t">
                                            <li>
                                                <p>Have a question?</p>
                                                <i class="far fa-arrow-right"></i> <a href="/contact-us"> Contact us</a>
                                            </li>
                                        </ul>
                                    </div>

                                </div>

                                <div id="hotels" class="confTab">
                                    <div class="col-10">
                                        <div>
                                            <h4 class="text-upper f-bold"><?php $h_title = get_field('h_title'); echo $h_title; ?></h4>
                                            <p> <?php $h_desc = get_field('h_description', $q_id); echo $h_desc;?></p>
                                        </div>
                                        
                                        <?php 
                                        $all_hotels = get_field('all_hotels', $q_id);
										// print_r($hotels);
                                        foreach ($all_hotels as $hotel) {
                                            $id = $hotel->ID; 
                                            $name = $hotel->post_title;
                                            $rate = get_field('rate',$id);
                                            $link = get_field('booking_link', $id);
                                            $img = get_the_post_thumbnail_url($id, 'full');

                                        ?>
                                        <div class="col-12 cards_h">
                                          <div class="col-6">
                                              <img src="<?php echo $img; ?>" class="h_img">
                                          </div>  
                                          <div class="col-6">
                                              <h4 class="text-upper f-bold mb-2"><?php echo $name; ?></h4>
                                              <p><?php echo $rate; ?></p>
                                              <button class="btn-default btn-yellow" onclick="location.href='<?php echo $link; ?>'">Book now</button>
                                          </div> 
                                        </div> 
                                       <?php    //// print_r($hotel);
                                        }
                                        ?>
                                        
                                    </div>
                                    <div class="col-2 has-sidebar">
                                        <ul class="side-t">
                                            <li>
                                                <p>Have a question?</p>
                                                <i class="far fa-arrow-right"></i> <a href="/contact-us"> Contact us</a>
                                            </li>
                                           
                                        </ul>
                                    </div>
                                   
                                </div>

                                <div id="faqs" class="confTab">
                                    <div class="col-10">
                                        <?php echo $faqs; ?>
                                    </div>
                                    <div class="col-2">
                                        <ul class="side-t">
                                            <li>
                                                <p>Have a question?</p>
                                                <i class="far fa-arrow-right"></i> <a href="/contact-us"> Contact us</a>
                                            </li>
                                           
                                        </ul>
                                    </div>
                                </div>

                                <div id="lets-get-social" class="confTab" >
                                    <div class="col-2"></div>
                                    <div class="col-8">
                                        <?php twitter_user_feed('RealCoinGeek'); ?>
                                    </div>
                                    <div class="col-2"></div>
                                </div>

                                <div id="sponsors-media-partners" class="confTab">
                                	<div class="col-12">
										<?php if($slug == 'sponsors-and-media-partners'){ ?>
											<h4 id="sponsorsHead" class="text-center text-upper f-bold mb-2">Sponsors</h4>
											<div class="col-12" id="sponsors-list">
												<div class="row tab-desc mb-2" style="display:none;">
													<h4><?php the_title(); ?> Speaker's team:</h4>
													<h4 class="mb-2">Chief Scientist, founder, ceo, cso and more</h4>
													<p> <?php $s_desc = get_field('speakers_intro', $q_id); echo $s_desc;?></p>
												</div>
												<div class="row">
													<?php //// print_r($sponsors); 
													foreach ($sponsors as $sponsor) { 
														$s = get_field('sponsor_or_media', $sponsor->ID); 
														if($s == 'Sponsor'){
																sponsor_info($sponsor->ID); 
														} ?>
												   <?php } ?>
												</div>
											</div>
											
											<h4 id="sponsorsHead" class="text-center text-upper f-bold mb-2">Media partners</h4>
											<div class="col-12" id="sponsors-list">
												<?php // print_r($sponsors); ?>
												<div class="row tab-desc mb-2" style="display:none;">
													<h4><?php the_title(); ?> Speaker's team:</h4>
													<h4 class="mb-2">Chief Scientist, founder, ceo, cso and more</h4>
													<p> <?php $s_desc = get_field('speakers_intro'); echo $s_desc;?></p>
												</div>
												<div class="row">
													<?php //// print_r($sponsors); 
													foreach ($sponsors as $sponsor) { 
														$s = get_field('sponsor_or_media', $sponsor->ID); 
														if($s == 'Media partner'){
															sponsor_info($sponsor->ID); 
														} ?>
												   <?php } ?>
												</div>
											</div>
										<?php } ?>
										<?php foreach ($sponsors as $sponsor) {
											if($slug == $sponsor->post_name){ 
												sponsor_profile($sponsor->ID); 
											}
										} ?>
                                    </div>
                                </div>

                                <div id="hackathon" class="confTab">
                                	<?php 
                                	$title_hack = get_field('hackathon_title', $q_id);
                                	$intro_hack = get_field('intro_hackthon', $q_id);
                                	$content_hack = get_field('content_hackthon', $q_id);
                                	$form_hack = get_field('form_hack', $q_id);
                                	?>
                                	<div class="col-12 text-center">
                                		<h4 class="text-upper f-bold mb-2"><?php echo $title_hack; ?></h4>
                                		<p><?php echo $intro_hack; ?></p>
                                		<div id="content_hackthon" class="mb-2"><?php echo $content_hack; ?></div>
                                		<div id="form_hack" class="p-all col-12 mb-2">
                                			<h4 class="text-upper f-bold mb-2 pt-2">How to enter</h4>
                                			<p class="mb-2">Registration kicks off now, to register please fill out this form and you will receive a welcome email with all the details on what to expect. At a later stage the resources you need to be ready to go on the day of the start of the event will become available on our website.</p>
                                			<div class="col-2"></div>
                                			<div class="col-8 form-default "><?php echo do_shortcode($form_hack); ?></div>
                                			<div class="col-2"></div>
                                		</div>
                                		<div class="col-12 pt-2">
                                			
                                			<div class="col-6 mb-2">
                                				<img src="https://coingeek.ga/wp-content/uploads/2019/03/what_is_hackthon.png" class="img-auto">
                                			</div>
                                			<div class="col-6 text-left mb-2">
                                				<h4 class="text-upper f-bold mb-2">What is a “Hackathon?”</h4>
                                				<p>Traditionally a hackathon is an event where participants from a variety of backgrounds team up to “hack” together creative solutions and program unique applications. This is a creative event where teams can show off the projects they built and compete for prizes. When being hosted at a physical location they usually last one or two days, whereas virtual Hackathons can range in the time allotted.</p>
                                			</div>
                                		</div>
                                	</div>

                                </div>                                
                            
                            </div>
                        </div>
                    <?php endwhile; endif; ?>
                </div>
            <?php if($template): ?>
        </div>
    <?php endif; ?>
    </div>
    <script>
        jQuery(function(){
            jQuery('#menu-item-120458 a').click(function(a)
            {
                jQuery('.box__applyS').css('display','block');
            });
            jQuery('#menu-conference_submenu .menu-item a').not('#menu-item-120458 a').click(function(a)
            {
                jQuery('.box__applyS').css('display','none');
            });
            //jQuery('.box__applyS').css('display','none');
            jQuery('#keep-informed input[type="submit"]').addClass('btn-default btn-yellow');
            
        });

    </script>
    <?php get_template_part('template_parts/apply_speaker');  ?>
    <?php get_template_part('template_parts/newsletters');  ?>

</main>
    <script>
        jQuery(function(){

            //Register Button to click Registration Tab
            jQuery('.register_btn').on('click', function(event){
                jQuery( "#menu-item-120459 a" ).click();
            });
            
        });

    </script>
<?php get_footer(); ?>