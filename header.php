<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="initial-scale=1.0,width=device-width,height=device-height,shrink-to-fit=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="google" content="notranslate">
    <?php wp_head(); ?>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?= get_bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=cyrillic,cyrillic-ext,latin-ext,vietnamese" rel="stylesheet">
	<!-- favicon -->
    <link rel="apple-touch-icon" sizes="114x114" href="<?= ASSETS_PATH ?>images/favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= ASSETS_PATH ?>images/favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= ASSETS_PATH ?>images/favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= ASSETS_PATH ?>images/favicon/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= ASSETS_PATH ?>images/favicon/apple-touch-icon-180x180.png">
    <link rel="apple-touch-icon" sizes="57x57" href="<?= ASSETS_PATH ?>images/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= ASSETS_PATH ?>images/favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= ASSETS_PATH ?>images/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= ASSETS_PATH ?>images/favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-startup-image" href="<?= ASSETS_PATH ?>images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= ASSETS_PATH ?>images/favicon/favicon-16x16.png">
    <link rel="icon" type="image/png" sizes="228x228" href="<?= ASSETS_PATH ?>images/favicon/coast-228x228.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= ASSETS_PATH ?>images/favicon/favicon-32x32.png">
    <link rel="manifest" href="<?= ASSETS_PATH ?>images/favicon/manifest.json">
    <link rel="shortcut icon" href="<?= ASSETS_PATH ?>images/favicon/favicon.ico">
    <link rel="yandex-tableau-widget" href="<?= ASSETS_PATH ?>images/favicon/yandex-browser-manifest.json">
    <meta name="msapplication-TileImage" content="<?= ASSETS_PATH ?>images/favicon/mstile-144x144.png">
    <meta name="msapplication-config" content="<?= ASSETS_PATH ?>images/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="apple-mobile-web-app-title" content="<?= get_bloginfo( 'name' ) ?>">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="application-name" content="<?= get_bloginfo( 'name' ) ?>">
    <!-- end favicon -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <script>var gulp_timer = Date.now();</script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-11325982-5"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-11325982-5');
    </script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-W58P9NJ');</script>
    <!-- End Google Tag Manager -->
    <meta name="msvalidate.01" content="C7C01BD10C73EE17BB880EF2F5BF1E3B" />
    <script type="text/javascript">
        jQuery(document).ready(function($){
            $('._form_5 style').detach();
        });
    </script>
</head>
<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W58P9NJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <header id="header">
        <div class="wrapper">
            <div class="header__container">
                <?php get_template_part('template_parts/header__logo'); ?>
                <?php get_template_part('template_parts/header__menu'); ?>
                <?php echo get_search_form(); ?>
                <?php get_template_part('template_parts/header__social'); ?>
            </div>
        </div>
        <div style="background-color:#141313; text-align: center; padding: 5px; font-size: 0.7rem;"><span style=" font-weight: bold;">SCHEDULED MAINTENANCE</span> | <span>May 18, 2019 - 08:30AM BST</span></div>
    </header>