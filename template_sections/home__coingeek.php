<?php
    $frontpage_id = get_option( 'page_on_front' );
    $active = get_field('active_coingeek_week', $frontpage_id );
    $term = get_field('category_post', $frontpage_id );
?>
<?php if($active): ?>
    <div class="home__section_content week__wrapper">
        <div class="wrapper">
            <div class="week__section">
                <div class="title">
                    <h2 class="title__section"><?php _e('Coingeek Week', DOMAIN); ?></h2>
                </div>
                <div class="row">
                    <div class="latest__new--left col-6">
                        <div class="row">
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'posts_per_page' => 2,
                                'tax_query' => [
                                    [
                                        'taxonomy' => 'post_tag',
                                        'terms' => $term
                                    ],
                                ],
                                // Rest of your arguments
                            );
                            $query = new WP_Query($args);
                            if($query->have_posts()):
                                while($query->have_posts()): $query->the_post();
                                    set_query_var( 'img_new_size', IMG_COINGEEK );
                                    ?>
                                    <div class="col-12">
                                        <?php get_template_part('template_parts/new'); ?>
                                    </div>

                                    <?php
                                endwhile;
                            endif;
                            wp_reset_query();
                            ?>
                        </div>
                    </div>
                    <div class="latest__new--right col-6">
                        <div class="row">
                            <div class="col-6">
                                <div class="lists__coingeek--week">
                                    <?php
                                        $args = array(
                                            'post_type' => 'post',
                                            'posts_per_page' => 4,
                                            'offset' => 2,
                                            'tax_query' => [
                                                [
                                                    'taxonomy' => 'post_tag',
                                                    'terms' => $term
                                                ],
                                            ],
                                            // Rest of your arguments
                                        );
                                        $query = new WP_Query($args);
                                        if($query->have_posts()):
                                            while($query->have_posts()): $query->the_post();
                                                set_query_var( 'img_new_size', IMG_BUSINESS_THUMB );
                                    ?>
                                        <?php get_template_part('template_parts/coingeek__week__item'); ?>
                                                <?php
                                            endwhile;
                                        endif;
                                    wp_reset_query();
                                    ?>
                                </div>
                            </div>
                            <div class="col-6">
                                <ul class="sidebar__image">
                                    <?php
                                    $args = array(
                                        'post_type' => 'post',
                                        'posts_per_page' => 4,
                                        'offset' => 2,
                                        'tax_query' => [
                                            [
                                                'taxonomy' => 'post_tag',
                                                'terms' => $term
                                            ],
                                        ],
                                        // Rest of your arguments
                                    );
                                    $query = new WP_Query($args);
                                    if($query->have_posts()):
                                        while($query->have_posts()): $query->the_post();
                                            $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), IMG_NEW_SIDEBAR );
                                            ?>
                                            <li>
                                                <img src="<?php echo $image_url[0]; ?>" alt="<?php the_title(); ?>">
                                            </li>
                                            <?php
                                        endwhile;
                                    endif;
                                    wp_reset_query();
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
    endif;
?>
